package com.qing.demo.listener;



import com.qing.demo.event.StageReadyEvent;
import javafx.stage.Stage;
import org.springframework.context.ApplicationListener;

public class StageInitializer implements ApplicationListener<StageReadyEvent> {

    @Override
    public void onApplicationEvent(StageReadyEvent event) {
        Stage stage = event.getStage();

        System.out.println("..............");
    }

}