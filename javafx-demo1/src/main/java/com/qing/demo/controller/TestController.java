package com.qing.demo.controller;

import com.github.spring.boot.javafx.stereotype.ViewController;
import com.qing.demo.service.UserService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@ViewController
public class TestController {



    @Autowired
    private UserService userService;

    @FXML
    public void login(ActionEvent event) {
        System.out.println("=============start===============");
        System.out.println(userService.getUser().toString());
        System.out.println("=============success===============");
    }


}
