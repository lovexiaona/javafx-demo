package com.qing.demo.view;

import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component("indexView")
public class IndexView {

    public Parent getIndexView(){
        Button btn = new Button("b1");
        Label lab=new Label("标签");
        Text t1=new Text("文本。。。");
        Text t2=new Text();
        t2.setText("文本2。。。");

        TextFlow tf=new TextFlow();
        tf.getChildren().addAll(t1,t2);

        ProgressBar pb=new ProgressBar();
        pb.setProgress(0.25);
        ImageView iv=new ImageView();
        iv.setImage(new Image("image/favicon.ico", 100, 100, true, true, true));

        RadioButton rb1=new RadioButton();
        RadioButton rb2=new RadioButton();
        rb2.setLayoutX(20);
        CheckBox cb=new CheckBox();
        cb.setLayoutX(40);
        Group group=new Group();
        group.getChildren().addAll(rb1,rb2,cb);

        TextField tfIn=new TextField();
        TextArea ta=new TextArea();
        PasswordField psf=new PasswordField();

        FlowPane pane=new FlowPane();
        pane.getChildren().addAll(btn,lab,tf,pb,iv,group,tfIn,ta,psf);
        return pane;
    }
}
