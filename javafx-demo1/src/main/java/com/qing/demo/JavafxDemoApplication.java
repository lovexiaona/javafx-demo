package com.qing.demo;

import com.github.spring.boot.javafx.SpringJavaFXApplication;
import com.qing.demo.event.StageReadyEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import java.util.Objects;
import org.springframework.context.ApplicationEvent;

@ComponentScan(basePackages ="com.qing.demo")
@SpringBootApplication
public class JavafxDemoApplication  extends SpringJavaFXApplication {



    @Override
    public void start(Stage primaryStage) throws Exception {
        /*IndexView indexView=new IndexView();
        Parent parent = indexView.getIndexView();
        Scene scene = new Scene(parent);

        primaryStage.setScene(scene);
        primaryStage.setY(100);
        primaryStage.setX(100);
        primaryStage.setWidth(1000);
        primaryStage.setHeight(600);
        primaryStage.show();*/

        Parent root= FXMLLoader.load(Objects.requireNonNull(getClass().getClassLoader().getResource("fxml/test.fxml")));
        primaryStage.setTitle("JavaFX Demo");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
//        applicationContext.publishEvent(new StageReadyEvent(primaryStage));
    }

    public static void main(String[] args) {
        launch(JavafxDemoApplication.class,args);
    }



}
