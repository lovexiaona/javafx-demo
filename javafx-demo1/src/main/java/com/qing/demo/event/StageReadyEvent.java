package com.qing.demo.event;

import javafx.stage.Stage;
import org.springframework.context.ApplicationEvent;

public class StageReadyEvent extends ApplicationEvent {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public StageReadyEvent(Stage stage) {
        super(stage);
    }

    public Stage getStage() {
        // TODO Auto-generated method stub
        return ((Stage)getSource());
    }
}
