参考网站

JavaFX中文官方网站

SpringBoot整合JavaFX进行桌面应用开发

JavaFX Scene Builder的详细下载与安装

JAVAFX 第三方库 布局 小工具 美化 测试 UI 框架

史上最直观的JavaFx布局讲解

https://zhuanlan.zhihu.com/p/540766457

SpringBoot整合JavaFx

https://blog.csdn.net/weixin_44480167/article/details/121145299

https://zhuanlan.zhihu.com/p/635191317

https://github.com/yoep/spring-boot-starter-javafx

https://github.com/yoep/spring-boot-starter-javafx/issues/1

https://blog.csdn.net/YCJ_xiyang/article/details/103383733

https://blog.csdn.net/qq_43604520/article/details/103995187

https://zhuanlan.zhihu.com/p/187673380



https://gitee.com/lwdillon/fx-falsework



exe包

https://github.com/fvarrui/JavaPackager

javafx的分层

一个javafx程序有三层，最外面的是Stage层，一个Stage就是一个独立的窗口。

在往里是Scene层，一个Scene就是一个窗口内部的一个状态。

在往里就是一个一个的node节点，节点可以是按钮、标签、文本等组件、



    Types 类型
    -----------------
    inherit 继承
    <boolean> 
    <string> 
    <number> 
    <integer>
    <size> 
    <length> 
    <percentage> 
    <angle> 
    <duration> 
    <point> 
    <color-stop> 
    <uri> 
    <effect> 
    <font> 
    <paint> 
    <color> 
    
    
    Nodes 节点
    ------------------
    Group 群
    Node 节点
    Pseudo-classes 伪类
    Parent 父母
    Scene 现场
    -------------------
    ImageView 图像视图
    AnchorPane 锚窗格
    BorderPane 边框窗格
    DialogPane 对话框窗格
    FlowPane 流窗格
    GridPane 网格窗格
    HBox 
    Pane 窗 格
    Region 地区
    StackPane 堆栈窗格
    TilePane 磁贴窗格
    VBox VBox
    MediaView 媒体视图
    
    --------------------
    Shape 形状
    Arc
    Circle 圈
    CubicCurve 立方曲线
    Ellipse 椭圆
    Line 线
    Path 路径
    Polygon 多边形
    QuadCurve 四曲线
    Rectangle 矩形
    SVGPath SVGPath
    Text 发短信
    WebView 网页视图
    
    --------------------
    Accordion 手风琴
    Button 按钮
    ButtonBase 按钮底座
    Cell 细胞
    CheckBox 复选框
    CheckMenuItem 检查菜单项
    ChoiceBox 选择框
    ColorPicker 颜色选择器
    ComboBox 组合框
    ComboBoxBase 组合框库
    ContextMenu 上下文菜单
    Control 控制
    DatePicker 日期选择器
    HTMLEditor HTMLEditor
    Hyperlink 超链接
    IndexedCell 索引单元格
    Label 标签
    Labeled 标记
    ListCell 列表单元格
    ListView 列表视图
    Menu 菜单
    MenuBar 菜单栏
    MenuButton 菜单按钮
    MenuItem 菜单项
    Pagination 分页
    PasswordField 密码字段
    PopupControl 弹出窗口控件
    ProgressBar 进度条
    ProgressIndicator 进度指示器
    RadioButton 单选按钮
    RadioMenuItem 单选菜单项
    ScrollBar 滚动条
    ScrollPane 滚动窗格
    Separator 分隔符
    Spinner 旋转
    Slider 滑 块
    SplitMenuButton 拆分菜单按钮
    SplitPane 拆分窗格
    TabPane 选项卡窗格
    TableColumnHeader 表列标题
    TableView 表视图
    TextArea 文本区域
    TextInputControl 文本输入控件
    TextField 文本字段
    TitledPane 标题窗格
    ToggleButton 切换按钮
    ToolBar 工具栏
    Tooltip 工具提示
    TreeCell 树细胞
    TreeTableCell 树表单元格
    TreeView 树视图
    
    Charts 图表
    --------------------
    AreaChart 面积图
    Axis 轴
    BarChart 条形图
    BubbleChart 气泡图
    CategoryAxis 类别轴
    Chart 图表
    Legend 传说
    LineChart 折线图
    NumberAxis 数字轴
    PieChart 饼图
    ScatterChart 散点图
    ValueAxis 价值轴
    XYChart 鑫驰克特
    
    
    
    





      // 1、坐标布局 设置元素距离上、下、左、右多少像素
            AnchorPane anchorP = new AnchorPane();
            anchorP.getChildren().addAll(btn1,btn2);
            AnchorPane.setLeftAnchor(btn1 , Double.valueOf(0));
            AnchorPane.setRightAnchor(btn2 , Double.valueOf(0));
            AnchorPane.setTopAnchor(btn2 , Double.valueOf(0));
    
            // 2、边框布局 把元素都设置到4个边缘的位置
            BorderPane borderPane = new BorderPane();
            borderPane.setTop(btn4);
            borderPane.setRight(btn5);
            borderPane.setCenter(anchorP);
    
            // 3、DialogPane 弹框布局，
            DialogPane dialogPane = new DialogPane();
            dialogPane.setHeaderText("标题");
            dialogPane.setContent(btn1);
    
            // 4、FlowPane 流式布局
            FlowPane flowPane = new FlowPane();
            flowPane.setHgap(40);// 设置水平间隙
            flowPane.setVgap(40);// 设置竖直间隙
            flowPane.setOrientation(Orientation.VERTICAL);// 设置竖直排列
            flowPane.getChildren().addAll(btn1,btn2,btn3,btn4);
    
            // 5、GridPane 网络布局 index 都是从0开始，
            //     如果不从0开始设置，就自动把最小index 设为了0的位置 
            GridPane gridPane = new GridPane();
            gridPane.setMinSize(100,100);
            GridPane.setColumnIndex(btn1 , 1);
            GridPane.setRowIndex(btn1,1);
            GridPane.setColumnIndex(btn2 , 1);
            GridPane.setRowIndex(btn2,2);
            GridPane.setColumnIndex(btn3 , 2);
            GridPane.setRowIndex(btn3,1);
            GridPane.setColumnIndex(btn4 , 2);
            GridPane.setRowIndex(btn4,2);
            GridPane.setColumnIndex(btn5 , 3);
            GridPane.setRowIndex(btn5,3);
            GridPane.setColumnIndex(btn6 , 4);
            GridPane.setRowIndex(btn6,4);
            // 这个网格布局还是建议用 fxml 来写吧，这代码看着挺别扭
            gridPane.getChildren().addAll(btn1,btn2,btn3,btn4,btn5,btn6);
    
            // 6、VBox 简单，就是竖着排下来，HBox 就是横着排下来的
            VBox vBox = new VBox();
            vBox.setAlignment(Pos.TOP_RIGHT);// 设置靠哪边
            vBox.setFillWidth(true);
            vBox.setSpacing(Double.valueOf(20));
            vBox.setMaxSize(120,10);
            vBox.getChildren().addAll(btn1,btn2,btn3,btn4,btn5,btn6);
    
            // 7、StackPane 堆叠布局 ，元素会叠在一起，
            //如果没给每一个元素单独设置位置，就叠在一起
            StackPane stackPane = new StackPane();
            stackPane.setAlignment(Pos.BOTTOM_CENTER);
            StackPane.setAlignment(btn1,Pos.CENTER_LEFT);
            stackPane.getChildren().addAll(btn1,btn2,btn3,btn4,btn5,btn6);
    
            // 8、TextFlow 这个应该不算布局，富文本可以用这个，
               //可以把文字、图片、按钮等弄在一起，很好用。
            TextFlow textFlow = new TextFlow();
            textFlow.setMaxSize(100,20);
            textFlow.setMaxWidth(100);
            Label label = new Label();
            label.setMaxWidth(100);
            label.setWrapText(true);
            label.setText("我 你");
            textFlow.getChildren().addAll(label);
    
            // 9、TilePane   砖块布局 其中一个块的大小，不影响其它块的大小
            TilePane tilePane = new TilePane();
            tilePane.setPrefRows(1);
            tilePane.setPrefColumns(2);
            tilePane.setHgap(2);
            tilePane.setVgap(3);
            TilePane.setMargin(btn1 , new Insets( 100,100,100,100));
            tilePane.getChildren().addAll(btn1,btn2,btn3,btn4,btn5,btn6);



SpringBoot整合Javafx

pom

    <?xml version="1.0" encoding="UTF-8"?>
    <project xmlns="http://maven.apache.org/POM/4.0.0"
             xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
             xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
        <modelVersion>4.0.0</modelVersion>
    
        <groupId>org.example</groupId>
        <artifactId>javafx-demo2</artifactId>
        <version>1.0-SNAPSHOT</version>
    
        <properties>
            <maven.compiler.source>8</maven.compiler.source>
            <maven.compiler.target>8</maven.compiler.target>
            <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
            <spring-boot.version>2.3.2.RELEASE</spring-boot.version>
        </properties>
    
        <dependencies>
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter</artifactId>
                <version>${spring-boot.version}</version>
            </dependency>
            <dependency>
                <groupId>com.github.yoep</groupId>
                <artifactId>spring-boot-starter-javafx</artifactId>
                <version>1.0.12</version>
            </dependency>
            <dependency>
                <groupId>org.projectlombok</groupId>
                <artifactId>lombok</artifactId>
                <version>1.18.26</version>
            </dependency>
            <dependency>
                <groupId>org.openjfx</groupId>
                <artifactId>javafx-controls</artifactId>
                <version>17.0.6</version>
            </dependency>
            <dependency>
                <groupId>org.openjfx</groupId>
                <artifactId>javafx-fxml</artifactId>
                <version>17.0.6</version>
            </dependency>
        </dependencies>
    
    </project>



    package com.qing.demo;
    
    import com.github.spring.boot.javafx.SpringJavaFXApplication;
    import com.github.spring.boot.javafx.stage.BorderlessStageWrapper;
    import com.github.spring.boot.javafx.view.ViewLoader;
    import com.qing.demo.service.UserService;
    import javafx.fxml.FXMLLoader;
    import javafx.scene.Parent;
    import javafx.scene.Scene;
    import javafx.stage.Stage;
    import org.springframework.boot.autoconfigure.SpringBootApplication;
    import org.springframework.context.annotation.ComponentScan;
    
    import java.util.Objects;
    
    @SpringBootApplication
    @ComponentScan("com.qing.demo.*")
    public class DemoApplication extends SpringJavaFXApplication {
    
        public static void main(String[] args) {
            launch(DemoApplication.class, args);
    
        }
    
    
        @Override
        public void start(Stage primaryStage) throws Exception {
            FXMLLoader fxmlLoader = new FXMLLoader(Objects.requireNonNull(getClass().getClassLoader().getResource("jfxml/index.fxml")));
    //       使springboot的注入生效
            fxmlLoader.setControllerFactory(applicationContext::getBean);
            Parent root= fxmlLoader.load();
    
            primaryStage.setTitle("JavaFX Demo");
            primaryStage.setScene(new Scene(root));
            primaryStage.show();
        }
    
    
    }



    <?xml version="1.0" encoding="UTF-8"?>
    
    <?import java.lang.*?>
    <?import java.util.*?>
    <?import javafx.scene.*?>
    <?import javafx.scene.control.*?>
    <?import javafx.scene.layout.*?>
    
    
    <AnchorPane prefHeight="400.0" prefWidth="600.0" xmlns="http://javafx.com/javafx/8" xmlns:fx="http://javafx.com/fxml/1" fx:controller="com.qing.demo.controller.IndexController">
       <children>
          <Button layoutX="259.0"  onAction="#longin" layoutY="172.0" mnemonicParsing="false" text="登录" />
       </children>
    </AnchorPane>
    



    package com.qing.demo.controller;
    
    import com.github.spring.boot.javafx.stereotype.ViewController;
    import com.qing.demo.service.UserService;
    import javafx.event.ActionEvent;
    import javafx.fxml.FXML;
    import org.springframework.beans.factory.annotation.Autowired;
    
    @ViewController
    public class IndexController {
    
        @Autowired
        private UserService userService;
    
        @FXML
        public void longin(ActionEvent actionEvent) {
            System.out.println("========start=============");
            System.out.println(actionEvent.getEventType().getName());
            System.out.println("userInfo:"+userService.getUserInfo().toString());
            System.out.println("=========end============");
        }
    }



    package com.qing.demo.config;
    
    import com.github.spring.boot.javafx.view.ViewLoader;
    import com.github.spring.boot.javafx.view.ViewLoaderImpl;
    import org.springframework.beans.factory.annotation.Autowired;
    import org.springframework.context.annotation.Bean;
    import org.springframework.context.annotation.Configuration;
    import org.springframework.context.support.ResourceBundleMessageSource;
    
    @Configuration
    public class ViewConfig {
    
    
    
        private ViewLoader viewLoader;
    
        @Bean
        public ResourceBundleMessageSource messageSource() {
            ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
            messageSource.setBasenames("languages/settings");
            return messageSource;
        }
    
    
        /*
        * 加载视图时有 2 个选项，通过 bean 自动选择控制器或手动定义 JavaFX 需要使用的控制器
        * */
        public void viewLoader() {
            viewLoader.load("jfxml/index.fxml");
        }
    
    }
    


