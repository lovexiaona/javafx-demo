package com.qing.demo;

import com.github.spring.boot.javafx.SpringJavaFXApplication;
import com.github.spring.boot.javafx.stage.BorderlessStageWrapper;
import com.github.spring.boot.javafx.view.ViewLoader;
import com.qing.demo.service.UserService;
import com.qing.demo.view.IndexView;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import java.util.Objects;

@SpringBootApplication
@ComponentScan("com.qing.demo.*")
public class DemoApplication extends SpringJavaFXApplication {

    public static void main(String[] args) {
        launch(DemoApplication.class, args);

    }


    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(Objects.requireNonNull(getClass().getClassLoader().getResource("jfxml/index.fxml")));
//       使springboot的注入生效
        fxmlLoader.setControllerFactory(applicationContext::getBean);
        Parent root= fxmlLoader.load();
        IndexView.initIndexViewStyle(root);
        Scene scene=new Scene(root);
        scene.getStylesheets().add(Objects.requireNonNull(getClass().getClassLoader().getResource("css/IndexStyle.css")).toExternalForm());
        primaryStage.setTitle("Pdf解析工具");
        primaryStage.setScene(scene);
        primaryStage.getIcons().add(new Image("images/cat-ioc.png"));
//        禁止窗口变化
        primaryStage.setResizable(false);
//        禁止最大化
        primaryStage.setMaximized(false);
        primaryStage.show();
    }


}