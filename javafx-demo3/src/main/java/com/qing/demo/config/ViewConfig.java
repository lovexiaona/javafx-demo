package com.qing.demo.config;

import com.github.spring.boot.javafx.view.ViewLoader;
import com.github.spring.boot.javafx.view.ViewLoaderImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;

@Configuration
public class ViewConfig {



    private ViewLoader viewLoader;

    @Bean
    public ResourceBundleMessageSource messageSource() {
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setBasenames("languages/settings");
        return messageSource;
    }


    /*
    * 加载视图时有 2 个选项，通过 bean 自动选择控制器或手动定义 JavaFX 需要使用的控制器
    * */
    public void viewLoader() {
        viewLoader.load("jfxml/index.fxml");
    }

}
