package com.qing.demo.controller;

import com.github.spring.boot.javafx.stereotype.ViewController;
import com.qing.demo.service.UserService;
import com.qing.demo.util.PDFSplitterUtils;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;

@ViewController
public class IndexController {

    @Autowired
    private UserService userService;

    @FXML
    private TextField targetFile;

    @FXML
    private TextField resourceFile;

    @FXML
    private Text message;


    @FXML
    public void submitButton(ActionEvent actionEvent) {
//        System.out.println("userInfo:"+userService.getUserInfo().toString());

        String sourceFolder=resourceFile.getText();
        String destinationFolder=targetFile.getText();
        if(StringUtils.isEmpty(sourceFolder)||StringUtils.isEmpty(destinationFolder)){
            message.setText("路径错误！！！");
            message.setStyle("-fx-text-fill:#f56c6c;");
            message.setFill(Color.rgb(245, 108, 108));
        }else{
            PDFSplitterUtils.splitPDF(sourceFolder,destinationFolder);
            message.setText("解析成功！！！");
            message.setStyle("-fx-text-fill:#67c23a;");
            message.setFill(Color.rgb(103, 194, 58));
        }

    }
}
