package com.qing.demo;

import com.github.spring.boot.javafx.SpringJavaFXApplication;
import com.github.spring.boot.javafx.stage.BorderlessStageWrapper;
import com.github.spring.boot.javafx.view.ViewLoader;
import com.qing.demo.service.UserService;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import java.util.Objects;

@SpringBootApplication
@ComponentScan("com.qing.demo.*")
public class DemoApplication extends SpringJavaFXApplication {

    public static void main(String[] args) {
        launch(DemoApplication.class, args);

    }


    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(Objects.requireNonNull(getClass().getClassLoader().getResource("jfxml/index.fxml")));
//       使springboot的注入生效
        fxmlLoader.setControllerFactory(applicationContext::getBean);
        Parent root= fxmlLoader.load();

        primaryStage.setTitle("JavaFX Demo");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }


}