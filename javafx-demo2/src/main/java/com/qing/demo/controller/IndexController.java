package com.qing.demo.controller;

import com.github.spring.boot.javafx.stereotype.ViewController;
import com.qing.demo.service.UserService;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import org.springframework.beans.factory.annotation.Autowired;

@ViewController
public class IndexController {

    @Autowired
    private UserService userService;

    @FXML
    public void longin(ActionEvent actionEvent) {
        System.out.println("========start=============");
        System.out.println(actionEvent.getEventType().getName());
        System.out.println("userInfo:"+userService.getUserInfo().toString());
        System.out.println("=========end============");
    }
}
